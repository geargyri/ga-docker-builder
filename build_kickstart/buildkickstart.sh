#!/bin/bash

function usage {
 echo "`basename $0` distro"
}

[ -z $1 ] && usage && exit 1

case $1 in
    7)
	DISTRO="cc7"
        CUSTOM_KS="build_kickstart/custom/cc7.template"
    ;;
    8|8a)
	DISTRO="c8"
        CUSTOM_KS="build_kickstart/custom/c8.template"
    ;;
    8s|8sa)
	DISTRO="cs8"
        CUSTOM_KS="build_kickstart/custom/c8s.template"
    ;;
    9s|9sa)
	DISTRO="cs9"
        CUSTOM_KS="build_kickstart/custom/c9s.template"
    ;;
    *)
        echo "distro $1 unknown, exiting"
	exit
    ;;
esac

KSFILE="${DISTRO}-base-docker.ks"
ANACONDA_LOG_DISTRO="${DISTRO}-base-docker"
COMMON_KS="build_kickstart/common.ks"

# echo $(pwd)
# echo $CUSTOM_KS
# echo $COMMON_KS

if [ -f $CUSTOM_KS ]; then

  # look for commands in template
  grep -q 'COMMANDS' $CUSTOM_KS
  if [ $? -eq 0 ]; then
    echo "commands and options found, adding to $KSFILE"
    echo "" >> $KSFILE
    sed -n '/##!COMMANDS_START/,/##!COMMANDS_END/p' $CUSTOM_KS > $KSFILE
  fi

  # look for packages in template
  grep -q '%packages' $CUSTOM_KS
  if [ $? -eq 0 ]; then
    echo "customised packages found, adding to $KSFILE"
    echo "" >> $KSFILE
    sed -n '/##!PACKAGES_START/,/##!PACKAGES_END/p' $CUSTOM_KS >> $KSFILE
  fi

  # look for pre-script in common
  grep -q '%pre' $COMMON_KS
  if [ $? -eq 0 ]; then
    echo "common pre-script found, adding to $KSFILE"
    echo "" >> $KSFILE
    sed -n '/##!PRESCRIPT_START/,/##!PRESCRIPT_END/p' $COMMON_KS >> $KSFILE
    sed -i "s/ANACONDA_LOG_DISTRO/${ANACONDA_LOG_DISTRO}/" $KSFILE
  fi

  # look for post-script in template
  grep -q '%post' $CUSTOM_KS
  if [ $? -eq 0 ]; then
    echo "custom postscript found, adding to $KSFILE"
    echo "" >> $KSFILE
    sed -n '/##!POSTSCRIPT_START/,/##!POSTSCRIPT_END/p' $CUSTOM_KS >> $KSFILE
  fi

  # look for post-script in common
  grep -q 'POSTSCRIPT' $COMMON_KS
  if [ $? -eq 0 ]; then
    echo "common postscript found, adding to $KSFILE"
    echo "" >> $KSFILE
    sed -n '/##!POSTSCRIPT_START/,/##!POSTSCRIPT_END/p' $COMMON_KS >> $KSFILE
  fi

fi
echo "Wrote $KSFILE for distro $DISTRO"
