### S1_COMMANDS
##!COMMANDS_START
# Distribution-specific
##!COMMANDS_END

### S2_PACKAGES
##!PACKAGES_START
# Distribution-specific
##!PACKAGES_END

### S3_PRESCRIPT
##!PRESCRIPT_START
%pre
ARCH=`uname -m`
cat >> /etc/rsyslog.conf  <<DELIM
\$template AnacondaTemplate, "<%PRI%>%TIMESTAMP:::date-rfc3339% image:ANACONDA_LOG_DISTRO-$ARCH %syslogtag:1:32%%msg:::sp-if-no-1st-sp%%msg%"
\$ActionForwardDefaultTemplate AnacondaTemplate

module(load="imfile" mode="inotify")
input(type="imfile"
  File="/tmp/anaconda.log"
  Tag="anaconda")
input(type="imfile"
  File="/tmp/dnf.librepo.log"
  Tag="dnf-librepo")
input(type="imfile"
  File="/tmp/packaging.log"
  Tag="packaging")
input(type="imfile"
  File="/tmp/ks-script-*.log"
  Tag="ks-pre")
input(type="imfile"
  File="/mnt/sysroot/root/ks-post.log"
  Tag="ks-post")

*.* @@linuxsoftadm.cern.ch:5014
DELIM
/usr/bin/systemctl restart rsyslog
%end
##!PRESCRIPT_END

### S4_POSTSCRIPT_1
# Distribution-specific

### S5_POSTSCRIPT_2
##!POSTSCRIPT_START
%post --nochroot
# Make rsyslog send everything before the reboot
pkill -HUP rsyslogd
sleep 30s
rm -rf /mnt/sysroot/root/ks-post.log
%end
##!POSTSCRIPT_END